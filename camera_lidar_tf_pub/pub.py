import math
import sys

from geometry_msgs.msg import TransformStamped

import numpy as np

import rclpy
from rclpy.node import Node

from tf2_ros.static_transform_broadcaster import StaticTransformBroadcaster

import PySimpleGUI as sg

# Define the layout of the GUI
layout = [
    [sg.Text('x'), sg.Slider(range=(-0.4, 0.4), resolution=0.01,orientation='h', size=(100, 30), default_value=0.16)],
    [sg.Text('y'), sg.Slider(range=(-0.4, 0.4), resolution=0.01,orientation='h', size=(100, 30), default_value=0.17)],
    [sg.Text('z'), sg.Slider(range=(-0.7, 0.4), resolution=0.01,orientation='h', size=(100, 30), default_value=-0.4)],
    [sg.Text('roll'), sg.Slider(range=(-3.15, 3.15), resolution=0.01,orientation='h', size=(100, 30), default_value=-1.34)],
    [sg.Text('pitch'), sg.Slider(range=(-3.15, 3.15), resolution=0.01,orientation='h', size=(100, 30), default_value=-0.03)],
    [sg.Text('yaw'), sg.Slider(range=(-3.15, 3.15), resolution=0.01,orientation='h', size=(100, 30), default_value=-1.69)],
]

window = sg.Window('tuner', layout)


def quaternion_from_euler(ai, aj, ak):
    ai /= 2.0
    aj /= 2.0
    ak /= 2.0
    ci = math.cos(ai)
    si = math.sin(ai)
    cj = math.cos(aj)
    sj = math.sin(aj)
    ck = math.cos(ak)
    sk = math.sin(ak)
    cc = ci*ck
    cs = ci*sk
    sc = si*ck
    ss = si*sk

    q = np.empty((4, ))
    q[0] = cj*sc - sj*cs
    q[1] = cj*ss + sj*cc
    q[2] = cj*cs - sj*sc
    q[3] = cj*cc + sj*ss

    return q


class StaticFramePublisher(Node):
    """
    Broadcast transforms that never change.

    This example publishes transforms from `world` to a static turtle frame.
    The transforms are only published once at startup, and are constant for all
    time.
    """

    def __init__(self):
        self.transformation = [0, 0, 0, 0, 0, 0, "lidar", "camera"]
        super().__init__('static_turtle_tf2_broadcaster')

        self.tf_static_broadcaster = StaticTransformBroadcaster(self)

        # Publish static transforms once at startup
        self.make_transforms()
        timer_period = 0.1  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)
        self.i = 0

    def timer_callback(self):
        
        event, values = window.read(30)
        if event == sg.WINDOW_CLOSED:
            exit()
        self.transformation = [float(values[0]),float(values[1]),float(values[2]),float(values[3]),float(values[4]),float(values[5]) ]
        self.make_transforms()

    def make_transforms(self):
        t = TransformStamped()

        t.header.stamp = self.get_clock().now().to_msg()
        t.header.frame_id = "lidar"
        t.child_frame_id = "oakd"

        t.transform.translation.x = float(self.transformation[0])
        t.transform.translation.y = float(self.transformation[1])
        t.transform.translation.z = float(self.transformation[2])
        quat = quaternion_from_euler(
            float(self.transformation[3]), float(self.transformation[4]), float(self.transformation[5]))
        t.transform.rotation.x = quat[0]
        t.transform.rotation.y = quat[1]
        t.transform.rotation.z = quat[2]
        t.transform.rotation.w = quat[3]

        self.tf_static_broadcaster.sendTransform(t)


def main():


    # pass parameters and initialize node
    rclpy.init()
    node = StaticFramePublisher()
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        pass

    rclpy.shutdown()
