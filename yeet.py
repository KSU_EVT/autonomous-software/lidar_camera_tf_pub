import PySimpleGUI as sg

# Define the layout of the GUI
layout = [
    [sg.Text('Slider 1'), sg.Slider(range=(0, 100), orientation='h', size=(20, 15), default_value=50)],
    [sg.Text('Slider 2'), sg.Slider(range=(0, 100), orientation='h', size=(20, 15), default_value=50)],
    [sg.Text('Slider 3'), sg.Slider(range=(0, 100), orientation='h', size=(20, 15), default_value=50)],
    [sg.Text('Slider 4'), sg.Slider(range=(0, 100), orientation='h', size=(20, 15), default_value=50)],
    [sg.Text('Slider 5'), sg.Slider(range=(0, 100), orientation='h', size=(20, 15), default_value=50)],
    [sg.Text('Slider 6'), sg.Slider(range=(0, 100), orientation='h', size=(20, 15), default_value=50)],
    [sg.Button('Submit')]
]

# Create the GUI window
window = sg.Window('Slider Demo', layout)

# Event loop to process GUI events
while True:
    event, values = window.read(30)
    if event == sg.WINDOW_CLOSED:
        break
        # Print the current values of the sliders
    print('Slider 1:', values[0])
    print('Slider 2:', values[1])
    print('Slider 3:', values[2])
    print('Slider 4:', values[3])
    print('Slider 5:', values[4])
    print('Slider 6:', values[5])

# Close the GUI window
window.close()